# Mann Island PHP Developer App
SOAP Web Service PHP Exercise

## Install
0. ensure soap extension is enabled in your php.ini
1. git clone the repository
2. cd /in/to/project/
3. install composer (https://getcomposer.org/download/)
4. run ```php composer.phar install```

## Accessing the service
### Running the service on a local php install (can be adapted for virtualised environments etc)
1. cd /in/to/project/
2. ensure service.wsdl is writable by the php process (as run below)
3. php -S localhost:8000

### Loading WSDL
- browse to localhost:8000/server.php?wsdl

### Develop your application
This service can be run in the background or in a separate terminal. It must be running for your application to call it, and will run until cancelled.

Develop your application separately to this service and have it treat the service as an external provider.

## Task Description
Using the quickly put together SOAP web service provided as your data source, create a basic web app within an PHP MVC framework of your choice (including a basic HTML frontend) that allows a user to do the following:

* Select from a list of available companies
* Obtain and display the company�s current stock price & directors names
* Save the company name & current stock quote to a MySQL database, showing a history of previous 5 results to the user. 
* One of the methods on the web service throws an intentional SoapFault, handle this exception within your app and notify the user with an appropriate error message.
* Remember that we will be focusing on PHP development, any HTML/CSS/JS skills demonstrated will be appreciated but will carry less weight given the work we do. This is your opportunity to directly show us your abilities and knowledge of backend development and associated tools.

## Credentials:
* username: php-exercise@mannisland.co.uk
* password: p455w0rd

## Notes:
We have setup a basic SOAP web service located above. The web service has three methods (getCompanies, getDirectorsBySymbol & getQuote). These methods will allow you to get the data required to develop your app.  Look up SoapClient (http://www.php.net/manual/en/book.soap.php) if you are unfamiliar with web services in PHP.

## Tips:
SoapUI will allow you to test the web service and its various methods from your desktop. 
