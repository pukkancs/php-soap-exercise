<?php

namespace Service;

class StockTicker
{
    /**
     * @param array $authentication
     * @return array[]
     * @throws \SoapFault
     */
    public function getCompanies($authentication)
    {
        $this->ensureAuthorised($authentication);

        return [
            [
                'name' => 'Google',
                'symbol' => 'GOOG'
            ],
            [
                'name' => 'Microsoft Corporation',
                'symbol' => 'MSFT'
            ],
            [
                'name' => 'IBM',
                'symbol' => 'IBM'
            ]
        ];
    }

    /**
     * @param array $authentication
     * @param string $symbol
     * @return string
     * @throws \SoapFault
     */
    public function getQuote($authentication, $symbol)
    {
        $this->ensureAuthorised($authentication);

        $companySymbols = [
            'GOOG',
            'MSFT',
            'IBM'
        ];

        if (in_array($symbol, $companySymbols)) {
            return sprintf("1.%'.02d", rand(1, 99));
        } else {
            throw new \SoapFault("Invalid Symbol", "Could not find stock price");
        }
    }

    /**
     * @param array $authentication
     * @param string $symbol
     * @throw \SoapFault
     */
    public function getDirectorsBySymbol($authentication, $symbol)
    {
        $this->ensureAuthorised($authentication);

        throw new \SoapFault("General Exception", "Intentional SoapFault thrown");
    }


    /**
     * @param array $authentication [0 => username, 1 => password]
     * @throws \SoapFault
     */
    private function ensureAuthorised($authentication)
    {
        $credentials = $this->getCredentials();
        list($username, $password) = $authentication;

        $authorised = ($username == $credentials['username'] && $password == $credentials['password']);

        if (!$authorised) {
            throw new \SoapFault("Authentication Error", "Could not find account related your username/password");
        }
    }


    private function getCredentials()
    {
        return [
            'username' => 'php-exercise@mannisland.co.uk',
            'password' => 'p455w0rd'
        ];
    }

}

