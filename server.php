<?php
require 'vendor/autoload.php';

use Zend\Soap\AutoDiscover;
use Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence;
use Service\StockTicker;

header('Content-type: application/xml');

if (isset($_GET['wsdl'])) {
    $host = $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
    $ServiceDiscover = (new AutoDiscover(new ArrayOfTypeSequence))
        ->setClass(StockTicker::class)
        ->setUri('http://'.$host.'/server.php')
        ->setServiceName('ExerciseWebService');

    $wsdl = $ServiceDiscover->generate();
    $wsdl->dump("service.wsdl");
    $dom = $wsdl->toDomDocument();
    echo $wsdl->toXml();

} else {
    // pointing to the current file here
    $soap = new Zend\Soap\Server("service.wsdl");
    $soap->setClass(StockTicker::class);
    $soap->handle();
}


